FROM elixir:1.8 as builder

ARG MIX_ENV=prod
ENV MIX_ENV=${MIX_ENV}

RUN apt update
RUN apt install postgresql-client -y
RUN apt autoclean -y

RUN mix local.hex --force
RUN mix local.rebar --force

WORKDIR /app

COPY . /app
RUN mix deps.get
RUN mix distillery.release --verbose

ENTRYPOINT /app/entrypoint.sh