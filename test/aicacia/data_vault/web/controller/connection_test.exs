defmodule Aicacia.DataVault.Web.Controller.ConnectionTest do
  use Aicacia.DataVault.Web.ConnCase

  alias Aicacia.DataVault.Service.Utils

  setup %{conn: conn} do
    {:ok,
     conn:
       conn
       |> put_req_header("accept", "application/json")}
  end

  describe "create" do
    test "create", %{conn: conn} do
      conn =
        post(conn, Routes.connection_path(conn, :create), %{
          "name" => "Test Connection",
          "type" => to_string(Utils.postgresql_type()),
          "params" => %{
            "username" =>
              Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:username],
            "password" =>
              Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:password],
            "uri" =>
              "postgresql://#{
                Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:hostname]
              }"
          }
        })

      connection_json = json_response(conn, 201)

      assert connection_json["name"] == "Test Connection"

      assert connection_json["username"] ==
               Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:username]

      assert connection_json["password"] ==
               Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:password]

      assert connection_json["uri"] ==
               "postgresql://#{
                 Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:hostname]
               }"
    end
  end
end
