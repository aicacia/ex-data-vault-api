defmodule Aicacia.DataVault.Web.View.ErrorTest do
  use Aicacia.DataVault.Web.ConnCase, async: true

  import Phoenix.View

  test "renders 404.json" do
    assert render(Aicacia.DataVault.Web.View.Error, "404.json", []) == %{errors: %{detail: "Not Found"}}
  end

  test "renders 500.json" do
    assert render(Aicacia.DataVault.Web.View.Error, "500.json", []) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
