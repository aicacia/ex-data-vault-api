use Mix.Config

config :aicacia_data_vault, Aicacia.DataVault.Web.Endpoint,
  http: [:inet6, port: 4000],
  url: [host: "localhost", port: 4000],
  ui_url: "https://data-vault.aicacia.com"

config :logger, level: :info

config :aicacia_data_vault, Aicacia.DataVault.Repo,
  database: "aicacia_data_vault_prod",
  pool_size: 15,
  show_sensitive_data_on_connection_error: false

config :cors_plug,
  origin: ["https://data-vault.aicacia.com"]

import_config "prod.secret.exs"
