use Mix.Config

config :aicacia_data_vault, Aicacia.DataVault.Web.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [],
  ui_url: "http://localhost:1234"

config :logger, :console, format: "[$level] $message\n"

config :phoenix, :stacktrace_depth, 20

config :phoenix, :plug_init_mode, :runtime

config :aicacia_data_vault, Aicacia.DataVault.Repo, database: "aicacia_data_vault_dev"
