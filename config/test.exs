use Mix.Config

config :aicacia_data_vault, Aicacia.DataVault.Web.Endpoint,
  http: [port: 4002],
  server: false,
  ui_url: "http://localhost:1234"

config :logger, level: :warn

config :aicacia_data_vault, Aicacia.DataVault.Repo,
  database: "aicacia_data_vault_test",
  pool: Ecto.Adapters.SQL.Sandbox
