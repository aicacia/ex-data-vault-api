use Mix.Config

config :aicacia_data_vault,
  ecto_repos: [Aicacia.DataVault.Repo],
  generators: [binary_id: true]

config :aicacia_data_vault, Aicacia.DataVault.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wF2JEGZt8htPRCCWxuDaB05lZDEhg8pIH72uX3LUsEW9JhnyBtOljer7JN4wyEdY",
  render_errors: [view: Aicacia.DataVault.Web.View.Error, accepts: ~w(json)],
  pubsub: [name: Aicacia.DataVault.PubSub, adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

config :aicacia_data_vault, Aicacia.DataVault.Repo,
  username: "postgres",
  password: "postgres",
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 10,
  show_sensitive_data_on_connection_error: true

config :aicacia_data_vault, Aicacia.DataVault.Scheduler, debug_logging: true

config :cors_plug,
  origin: ~r/.*/,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"]

import_config "#{Mix.env()}.exs"
