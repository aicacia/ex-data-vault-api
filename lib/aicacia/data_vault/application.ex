defmodule Aicacia.DataVault.Application do
  use Application
  import Supervisor.Spec, warn: false

  def start(_type, _args) do
    children = [
      Aicacia.DataVault.Repo,
      Aicacia.DataVault.Web.Endpoint,
      worker(Aicacia.DataVault.Scheduler, [])
    ]

    children =
      if Mix.env() != :test,
        do:
          children ++
            [worker(Task, [&Aicacia.DataVault.Service.Extractor.Run.init/0], restart: :temporary)],
        else: children

    opts = [strategy: :one_for_one, name: Aicacia.DataVault.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Aicacia.DataVault.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
