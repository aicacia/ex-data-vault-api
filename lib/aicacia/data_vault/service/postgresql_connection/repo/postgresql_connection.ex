defmodule Aicacia.DataVault.Service.PostgresqlConnection.Repo.PostgresqlConnection do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.PostgresqlConnection

  def get(id), do: Repo.get(PostgresqlConnection, id)
  def get!(id), do: Repo.get!(PostgresqlConnection, id)
  def get_by(search), do: Repo.get_by(PostgresqlConnection, search)
  def get_by!(search), do: Repo.get_by!(PostgresqlConnection, search)

  def create!(%{} = attrs) do
    %PostgresqlConnection{}
    |> cast(attrs, [:connection_id, :username, :password, :uri])
    |> validate_required([:connection_id, :username, :password, :uri])
    |> foreign_key_constraint(:connection_id)
    |> Repo.insert!()
  end

  def update!(%PostgresqlConnection{} = postgresql_connection, attrs \\ %{}) do
    postgresql_connection
    |> cast(attrs, [:username, :password, :uri])
    |> validate_required([:username, :password, :uri])
    |> Repo.update!()
  end

  def delete!(%PostgresqlConnection{} = postgresql_connection) do
    Repo.delete!(postgresql_connection)
  end
end
