defmodule Aicacia.DataVault.Service.PostgresqlConnection.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlConnection.Create
  alias Aicacia.DataVault.Service.PostgresqlConnection.Repo

  schema "" do
    field(:connection_id, :id)
    field(:username, :string)
    field(:password, :string)
    field(:uri, :string)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:connection_id, :username, :password, :uri])
    |> validate_required([:connection_id, :username, :password, :uri])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      Repo.PostgresqlConnection.create!(command)
    end)
  end
end
