defmodule Aicacia.DataVault.Service.PostgresqlConnection.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlConnection.Update
  alias Aicacia.DataVault.Service.PostgresqlConnection.Repo

  schema "" do
    field(:postgresql_connection_id, :id)
    field(:username, :string)
    field(:password, :string)
    field(:uri, :string)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:postgresql_connection_id, :username, :password, :uri])
    |> validate_required([:postgresql_connection_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      postgresql_connection = Repo.PostgresqlConnection.get!(command.postgresql_connection_id)

      Repo.PostgresqlConnection.update!(
        postgresql_connection,
        command
      )
    end)
  end
end
