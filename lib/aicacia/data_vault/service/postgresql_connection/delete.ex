defmodule Aicacia.DataVault.Service.PostgresqlConnection.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlConnection.Delete
  alias Aicacia.DataVault.Service.PostgresqlConnection.Repo

  schema "" do
    field(:postgresql_connection_id, :id)
  end

  def changeset(%{} = params) do
      %Delete{}
      |> cast(params, [:postgresql_connection_id])
      |> validate_required([:postgresql_connection_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      postgresql_connection = Repo.PostgresqlConnection.get!(command.postgresql_connection_id)

      Repo.PostgresqlConnection.delete!(postgresql_connection)

      postgresql_connection
    end)
  end
end
