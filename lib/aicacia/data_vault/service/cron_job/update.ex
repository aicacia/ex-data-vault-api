defmodule Aicacia.DataVault.Service.CronJob.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.CronJob.{Update, Validations, Repo}
  alias Aicacia.DataVault.Service

  schema "" do
    field(:cron_job_id, :id)
    field(:name, :string)
    field(:schedule, :string)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:cron_job_id, :name, :schedule])
    |> validate_required([:cron_job_id])
    |> Validations.validate_schedule()
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      cron_job =
        Repo.CronJob.get!(command.cron_job_id)
        |> Repo.CronJob.update!(command)

      Repo.Extractor.all_by_cron_job(cron_job.id)
      |> Enum.each(fn extractor ->
        Service.Extractor.Run.delete_job(extractor)
        Service.Extractor.Run.add_job!(cron_job, extractor)
      end)

      cron_job
    end)
  end
end
