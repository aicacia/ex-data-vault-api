defmodule Aicacia.DataVault.Service.CronJob.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.CronJob.{Repo, Create, Validations}

  schema "" do
    field(:name, :string)
    field(:schedule, :string)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:name, :schedule])
    |> Validations.validate_schedule()
    |> validate_required([:name])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      cron_job = Repo.CronJob.create!(command)
      cron_job
    end)
  end
end
