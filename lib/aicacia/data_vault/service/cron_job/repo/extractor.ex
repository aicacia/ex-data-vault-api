defmodule Aicacia.DataVault.Service.CronJob.Repo.Extractor do
  import Ecto.Query, warn: false

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.Extractor

  def all_by_cron_job_query(cron_job_id),
    do:
      from(u in Extractor,
        where: u.cron_job_id == ^cron_job_id,
        select: u
      )

  def all_by_cron_job(cron_job_id),
    do:
      all_by_cron_job_query(cron_job_id)
      |> Repo.all()
end
