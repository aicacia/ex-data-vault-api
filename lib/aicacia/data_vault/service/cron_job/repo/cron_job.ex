defmodule Aicacia.DataVault.Service.CronJob.Repo.CronJob do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.CronJob

  def get(id), do: Repo.get(CronJob, id)
  def get!(id), do: Repo.get!(CronJob, id)
  def get_by(search), do: Repo.get_by(CronJob, search)
  def get_by!(search), do: Repo.get_by!(CronJob, search)

  def all(), do: Repo.all(CronJob)

  def create!(%{} = attrs) do
    %CronJob{}
    |> cast(attrs, [:name, :schedule])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%CronJob{} = cron_job, attrs \\ %{}) do
    cron_job
    |> cast(attrs, [:name, :schedule])
    |> Repo.update!()
  end

  def delete!(%CronJob{} = cron_job) do
    Repo.delete!(cron_job)
  end
end
