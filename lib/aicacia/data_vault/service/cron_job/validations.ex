defmodule Aicacia.DataVault.Service.CronJob.Validations do
  import Ecto.Changeset

  def validate_schedule(%Ecto.Changeset{} = changeset) do
    schedule = get_field(changeset, :schedule)

    if schedule == nil do
      changeset
    else
      case Crontab.CronExpression.Parser.parse(schedule) do
        {:ok, _cron_expression} ->
          changeset

        {:error, message} ->
          changeset
          |> add_error(:schedule, message, validation: :schedule)
      end
    end
  end
end
