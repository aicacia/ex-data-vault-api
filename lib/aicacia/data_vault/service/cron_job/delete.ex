defmodule Aicacia.DataVault.Service.CronJob.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.CronJob.Delete
  alias Aicacia.DataVault.Service.CronJob.Repo

  schema "" do
    field(:cron_job_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:cron_job_id])
    |> validate_required([:cron_job_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      cron_job = Repo.CronJob.get!(command.cron_job_id)

      Repo.CronJob.delete!(cron_job)

      cron_job
    end)
  end
end
