defmodule Aicacia.DataVault.Service.TableField.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.TableField.Update
  alias Aicacia.DataVault.Service.TableField.Repo

  schema "" do
    field(:table_field_id, :id)
    field(:name, :string)
    field(:type, :string)
    field(:reference_id, :id)
    field(:tracked, :boolean)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:table_field_id, :name, :type, :reference_id, :tracked])
    |> validate_required([:table_field_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      Repo.TableField.get!(command.table_field_id)
      |> Repo.TableField.update!(command)
    end)
  end
end
