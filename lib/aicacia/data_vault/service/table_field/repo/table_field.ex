defmodule Aicacia.DataVault.Service.TableField.Repo.TableField do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.TableField
  alias Aicacia.DataVault.Service.Utils

  def get(id), do: Repo.get(TableField, id)
  def get!(id), do: Repo.get!(TableField, id)
  def get_by(search), do: Repo.get_by(TableField, search)
  def get_by!(search), do: Repo.get_by!(TableField, search)

  def get_reference_query() do
    from(tf in TableField,
      preload: [:table],
      select: tf
    )
  end

  def get_all_query() do
    reference_query = get_reference_query()

    from(tf in TableField,
      preload: [reference: ^reference_query],
      select: tf
    )
  end

  def create!(%{} = attrs) do
    %TableField{}
    |> cast(attrs, [:name, :type, :table_id, :reference_id, :primary_key])
    |> Utils.validate_valid_db_name(:name)
    |> validate_required([:name, :type, :table_id])
    |> foreign_key_constraint(:table_id)
    |> foreign_key_constraint(:reference_id)
    |> Repo.insert!()
  end

  def update!(%TableField{} = table_field, attrs \\ %{}) do
    table_field
    |> cast(attrs, [:name, :type, :reference_id])
    |> Utils.validate_valid_db_name(:name)
    |> validate_required([:name, :type])
    |> unique_constraint(:name, name: :table_fields_table_id_name)
    |> foreign_key_constraint(:reference_id)
    |> Repo.update!()
  end

  def delete!(%TableField{} = table_field) do
    Repo.delete!(table_field)
  end
end
