defmodule Aicacia.DataVault.Service.TableField.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.TableField.Delete
  alias Aicacia.DataVault.Service.TableField.Repo

  schema "" do
    field(:table_field_id, :id)
  end

  def changeset(%{} = params) do
      %Delete{}
      |> cast(params, [:table_field_id])
      |> validate_required([:table_field_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      table_field = Repo.TableField.get!(command.table_field_id)

      Repo.TableField.delete!(table_field)

      table_field
    end)
  end
end
