defmodule Aicacia.DataVault.Service.TableField.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.TableField.Create
  alias Aicacia.DataVault.Service.TableField.Repo

  schema "" do
    field(:name, :string)
    field(:type, :string)
    field(:table_id, :id)
    field(:reference_id, :id)
    field(:primary_key, :boolean, default: false)
    field(:tracked, :boolean, default: true)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:name, :type, :table_id, :reference_id, :tracked, :primary_key])
    |> validate_required([:name, :table_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      Repo.TableField.create!(command)
    end)
  end
end
