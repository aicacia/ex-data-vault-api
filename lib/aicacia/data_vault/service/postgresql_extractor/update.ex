defmodule Aicacia.DataVault.Service.PostgresqlExtractor.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlExtractor.Update
  alias Aicacia.DataVault.Service.PostgresqlExtractor.Repo

  schema "" do
    field(:postgresql_extractor_id, :id)
    field(:query, :string)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:postgresql_extractor_id, :query])
    |> validate_required([:postgresql_extractor_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      postgresql_extractor = Repo.PostgresqlExtractor.get!(command.postgresql_extractor_id)

      Repo.PostgresqlExtractor.update!(
        postgresql_extractor,
        command
      )
    end)
  end
end
