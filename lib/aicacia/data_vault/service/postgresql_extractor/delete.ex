defmodule Aicacia.DataVault.Service.PostgresqlExtractor.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlExtractor.Delete
  alias Aicacia.DataVault.Service.PostgresqlExtractor.Repo

  schema "" do
    field(:postgresql_extractor_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:postgresql_extractor_id])
    |> validate_required([:postgresql_extractor_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      postgresql_extractor = Repo.PostgresqlExtractor.get!(command.postgresql_extractor_id)

      Repo.PostgresqlExtractor.delete!(postgresql_extractor)

      postgresql_extractor
    end)
  end
end
