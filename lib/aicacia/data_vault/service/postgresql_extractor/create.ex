defmodule Aicacia.DataVault.Service.PostgresqlExtractor.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.PostgresqlExtractor.Create
  alias Aicacia.DataVault.Service.PostgresqlExtractor.Repo

  schema "" do
    field(:extractor_id, :id)
    field(:query, :string)
  end

  def changeset(%{} = params) do
      %Create{}
      |> cast(params, [:extractor_id, :query])
      |> validate_required([:extractor_id, :query])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      Repo.PostgresqlExtractor.create!(command)
    end)
  end
end
