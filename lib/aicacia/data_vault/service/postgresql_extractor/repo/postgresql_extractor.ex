defmodule Aicacia.DataVault.Service.PostgresqlExtractor.Repo.PostgresqlExtractor do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.PostgresqlExtractor

  def get(id), do: Repo.get(PostgresqlExtractor, id)
  def get!(id), do: Repo.get!(PostgresqlExtractor, id)
  def get_by(search), do: Repo.get_by(PostgresqlExtractor, search)
  def get_by!(search), do: Repo.get_by!(PostgresqlExtractor, search)

  def create!(%{} = attrs) do
    %PostgresqlExtractor{}
    |> cast(attrs, [:extractor_id, :query])
    |> validate_required([:extractor_id, :query])
    |> foreign_key_constraint(:extractor_id)
    |> Repo.insert!()
  end

  def update!(%PostgresqlExtractor{} = postgresql_extractor, attrs \\ %{}) do
    postgresql_extractor
    |> cast(attrs, [:query])
    |> validate_required([:query])
    |> Repo.update!()
  end

  def delete!(%PostgresqlExtractor{} = postgresql_extractor) do
    Repo.delete!(postgresql_extractor)
  end
end
