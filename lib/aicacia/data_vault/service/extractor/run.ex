defmodule Aicacia.DataVault.Service.Extractor.Run do
  require Logger

  alias Aicacia.DataVault.Model.{Extractor, CronJob, PostgresqlExtractor, PostgresqlConnection}
  alias Aicacia.DataVault.Service

  def init() do
    Service.Extractor.Repo.Extractor.all_with_cron_job()
    |> Enum.each(fn %Extractor{cron_job: cron_job} = extractor ->
      add_job!(cron_job, extractor)
    end)
  end

  def create_job!(%CronJob{schedule: schedule}, %Extractor{id: id, name: name}) do
    Aicacia.DataVault.Scheduler.new_job()
    |> Quantum.Job.set_name(job_name(name))
    |> Quantum.Job.set_schedule(Crontab.CronExpression.Parser.parse!(schedule))
    |> Quantum.Job.set_task({Aicacia.DataVault.Service.Extractor.Run, :call, [id]})
  end

  def job_name(name), do: name |> Base.encode16() |> String.to_atom()

  def add_job!(%CronJob{} = cron_job, %Extractor{} = extractor) do
    job = create_job!(cron_job, extractor)
    :ok = Aicacia.DataVault.Scheduler.add_job(job)
  end

  def delete_job(%Extractor{name: name}) do
    Aicacia.DataVault.Scheduler.delete_job(job_name(name))
  end

  def call(extractor_id) do
    Aicacia.DataVault.Repo.transaction(fn ->
      # TODO: this is crap locking
      extractor = Service.Extractor.Repo.Extractor.get_by(id: extractor_id, locked: false)

      if extractor != nil do
        extractor = Service.Extractor.Repo.Extractor.update!(extractor, %{locked: true})
        connection = Service.Connection.Repo.Connection.get!(extractor.connection_id)

        run_extractor!(
          Service.Connection.Repo.Connection.get_type!(connection),
          Service.Extractor.Repo.Extractor.get_type!(extractor),
          Service.Mapper.Repo.Mapper.all(extractor.id)
        )

        Service.Extractor.Repo.Extractor.update!(extractor, %{locked: false})
      end
    end)
  end

  defp run_extractor!(
         %PostgresqlConnection{username: username, password: password, uri: uri},
         %PostgresqlExtractor{query: query},
         mappers
       ) do
    %URI{host: hostname, path: path, port: port} = URI.parse(uri)

    {:ok, postgrex_pid} =
      Postgrex.start_link(
        hostname: hostname,
        port: port,
        database: String.replace(path, ~r/\/+/, ""),
        username: username,
        password: password
      )

    %Postgrex.Result{
      columns: columns,
      rows: rows
    } = Postgrex.query!(postgrex_pid, query, [])

    Enum.each(mappers, fn mapper ->
      table = Service.Table.Repo.Table.get!(mapper.table_id)
      {mapped_column_names, column_index_to_field_map} = map_columns(columns, mapper.fields)

      insert_sql =
        "INSERT INTO #{table.name} (#{Enum.join(mapped_column_names ++ ["inserted_at"], ", ")}) VALUES #{
          rows
          |> Enum.map(fn row ->
            "(#{render_row(column_index_to_field_map, row)})"
          end)
          |> Enum.join(", ")
        } ON CONFLICT DO NOTHING"

      Logger.debug(insert_sql)

      %Postgrex.Result{command: :insert} =
        Ecto.Adapters.SQL.query!(
          Aicacia.DataVault.Repo,
          insert_sql,
          []
        )
    end)

    GenServer.stop(postgrex_pid)
  end

  defp render_row(column_index_to_field_map, row) do
    values =
      column_index_to_field_map
      |> Enum.map(fn {index, _field} ->
        to_sql(Enum.at(row, index))
      end)

    (values ++ [to_sql(DateTime.utc_now())]) |> Enum.join(", ")
  end

  defp map_columns(columns, mapper_fields) do
    from_to_map =
      Enum.reduce(mapper_fields, %{}, fn mapper_field, acc ->
        Map.put(acc, mapper_field.from, mapper_field)
      end)

    column_index_to_field_map =
      columns
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {column, index}, acc ->
        Map.put(acc, index, Map.get(from_to_map, column))
      end)

    mapped_column_names =
      column_index_to_field_map
      |> Enum.map(fn {_index, field} -> field.to.name end)

    {mapped_column_names, column_index_to_field_map}
  end

  defp to_sql(value) when is_binary(value) or is_map(value) do
    "'#{value}'"
  end

  defp to_sql(value) do
    value
  end
end
