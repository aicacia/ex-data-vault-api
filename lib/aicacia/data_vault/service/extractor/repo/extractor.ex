defmodule Aicacia.DataVault.Service.Extractor.Repo.Extractor do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.Extractor
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  def get(id), do: Repo.get(Extractor, id)
  def get!(id), do: Repo.get!(Extractor, id)
  def get_by(search), do: Repo.get_by(Extractor, search)
  def get_by!(search), do: Repo.get_by!(Extractor, search)

  def all_query(connection_id),
    do:
      from(u in Extractor,
        where: u.connection_id == ^connection_id,
        select: u
      )

  def all_with_cron_job_query(),
    do:
      from(u in Extractor,
        preload: [:cron_job],
        select: u
      )

  def all(connection_id),
    do:
      all_query(connection_id)
      |> Repo.all()
      |> Enum.map(fn connection ->
        {connection, get_type!(connection)}
      end)

  def all_with_cron_job(),
    do:
      all_with_cron_job_query()
      |> Repo.all()

  def get_with_type!(id) do
    extractor = get!(id)
    {extractor, get_type!(extractor)}
  end

  def get_type!(%Extractor{id: id, type: type}) do
    if type == to_string(Utils.postgresql_type()) do
      Service.PostgresqlExtractor.Repo.PostgresqlExtractor.get_by!(extractor_id: id)
    else
      raise "Invalid extractor type #{type}"
    end
  end

  def create!(%{} = attrs) do
    %Extractor{}
    |> cast(attrs, [:connection_id, :cron_job_id, :name, :type])
    |> validate_required([:connection_id, :cron_job_id, :name, :type])
    |> foreign_key_constraint(:connection_id)
    |> foreign_key_constraint(:cron_job_id)
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%Extractor{} = extractor, attrs \\ %{}) do
    extractor
    |> cast(attrs, [:connection_id, :cron_job_id, :name, :locked])
    |> foreign_key_constraint(:connection_id)
    |> foreign_key_constraint(:cron_job_id)
    |> unique_constraint(:name)
    |> Repo.update!()
  end

  def delete!(%Extractor{} = extractor) do
    Repo.delete!(extractor)
  end
end
