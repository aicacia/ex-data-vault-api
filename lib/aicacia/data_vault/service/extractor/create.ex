defmodule Aicacia.DataVault.Service.Extractor.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Extractor.Create
  alias Aicacia.DataVault.Service.Extractor.Run
  alias Aicacia.DataVault.Service.Extractor.Repo
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  schema "" do
    field(:connection_id, :id)
    field(:cron_job_id, :id)
    field(:name, :string)
    field(:type, :string)
    field(:params, :map, default: %{})
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:connection_id, :cron_job_id, :name, :type, :params])
    |> Utils.string_key_to_atom_map(:params)
    |> validate_required([:name, :type])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      connection = Service.Connection.Repo.Connection.get!(command.connection_id)

      if connection.type != command.type do
        raise "Invalid extractor to connection type, connection = #{connection.type} extractor = #{
                command.type
              }"
      end

      extractor = Repo.Extractor.create!(command)
      cron_job = Service.CronJob.Repo.CronJob.get!(extractor.cron_job_id)

      type = String.to_atom(command.type)

      extractor_type =
        if type == Utils.postgresql_type() do
          {:ok, command} =
            Service.PostgresqlExtractor.Create.new(
              Map.merge(command.params, %{
                extractor_id: extractor.id
              })
            )

          {:ok, postgresql_extractor} = Service.PostgresqlExtractor.Create.handle(command)
          postgresql_extractor
        else
          raise "Invalid extractor type #{command.type}"
        end

      Run.add_job!(cron_job, extractor)

      extractor_type
    end)
  end
end
