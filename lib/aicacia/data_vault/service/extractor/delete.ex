defmodule Aicacia.DataVault.Service.Extractor.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Extractor.Delete
  alias Aicacia.DataVault.Service.Extractor.Run
  alias Aicacia.DataVault.Service.Extractor.Repo

  schema "" do
    field(:extractor_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:extractor_id])
    |> validate_required([:extractor_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      extractor = Repo.Extractor.get!(command.extractor_id)

      Repo.Extractor.delete!(extractor)
      Run.delete_job(extractor)

      extractor
    end)
  end
end
