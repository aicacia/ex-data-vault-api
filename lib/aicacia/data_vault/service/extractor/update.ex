defmodule Aicacia.DataVault.Service.Extractor.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Extractor.Update
  alias Aicacia.DataVault.Service.Extractor.Repo
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  schema "" do
    field(:extractor_id, :id)
    field(:cron_job_id, :id)
    field(:name, :string)
    field(:params, :map, default: %{})
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:extractor_id, :cron_job_id, :name, :params])
    |> Utils.string_key_to_atom_map(:params)
    |> validate_required([:extractor_id, :cron_job_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      extractor = Repo.Extractor.get!(command.extractor_id)

      extractor =
        Repo.Extractor.update!(
          extractor,
          command
        )

      if command.params != %{} do
        type = String.to_atom(command.type)

        if type == Utils.postgresql_type() do
          with {:ok, command} <-
                 Service.PostgresqlExtractor.Update.new(
                   Map.merge(command.params, %{
                     extractor_id: extractor.id
                   })
                 ) do
            with {:ok, postgresql_extractor} <- Service.PostgresqlExtractor.Update.handle(command) do
              postgresql_extractor
            end
          end
        else
          raise "Invalid extractor type #{extractor.type}"
        end
      else
        Repo.Extractor.get_type!(extractor)
      end
    end)
  end
end
