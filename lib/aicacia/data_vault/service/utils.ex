defmodule Aicacia.DataVault.Service.Utils do
  import Ecto.Changeset

  @re_invalid_db_name_chars ~r/[^\w]+/

  def validate_valid_db_name(%Ecto.Changeset{} = changeset, field) do
    db_name =
      get_field(changeset, field)
      |> String.downcase()
      |> String.replace(@re_invalid_db_name_chars, "")

    put_change(changeset, field, db_name)
  end

  def postgresql_type(), do: :postgresql

  def validate_connection_type(%Ecto.Changeset{} = changeset, field) do
    type = get_field(changeset, field, postgresql_type())

    if type == postgresql_type() do
      put_change(changeset, field, type)
    else
      add_error(changeset, field, "invalid connection type", type: type)
    end
  end

  def string_key_to_atom_map(%Ecto.Changeset{} = changeset, field) do
    string_key_map = get_field(changeset, field, %{})
    atom_key_map = for {key, val} <- string_key_map, into: %{}, do: {string_to_atom(key), val}
    put_change(changeset, field, atom_key_map)
  end

  defp string_to_atom(key) when is_binary(key) do
    String.to_atom(key)
  end

  defp string_to_atom(key) do
    key
  end
end
