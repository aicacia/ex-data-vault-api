defmodule Aicacia.DataVault.Service.Connection.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Connection.Delete
  alias Aicacia.DataVault.Service.Connection.Repo

  schema "" do
    field(:connection_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:connection_id])
    |> validate_required([:connection_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      connection = Repo.Connection.get!(command.connection_id)

      Repo.Connection.delete!(connection)

      connection
    end)
  end
end
