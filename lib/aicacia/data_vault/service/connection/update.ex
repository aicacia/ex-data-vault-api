defmodule Aicacia.DataVault.Service.Connection.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Connection.Update
  alias Aicacia.DataVault.Service.Connection.Repo
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  schema "" do
    field(:connection_id, :id)
    field(:name, :string)
    field(:params, :map, default: %{})
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:connection_id, :name, :params])
    |> Utils.string_key_to_atom_map(:params)
    |> validate_required([:connection_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      connection = Repo.Connection.get!(command.connection_id)

      connection =
        Repo.Connection.update!(
          connection,
          command
        )

      if command.params != %{} do
        type = String.to_atom(command.type)

        if type == Utils.postgresql_type() do
          {:ok, command} =
            Service.PostgresqlConnection.Update.new(
              Map.merge(command.params, %{
                connection_id: connection.id
              })
            )

          {:ok, postgresql_connection} = Service.PostgresqlConnection.Update.handle(command)
          {connection, postgresql_connection}
        else
          raise "Invalid connection type #{connection.type}"
        end
      else
        {connection, Repo.Connection.get_type!(connection)}
      end
    end)
  end
end
