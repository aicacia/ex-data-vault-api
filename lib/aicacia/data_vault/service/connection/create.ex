defmodule Aicacia.DataVault.Service.Connection.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Connection.Create
  alias Aicacia.DataVault.Service.Connection.Repo
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  schema "" do
    field(:name, :string)
    field(:type, :string)
    field(:params, :map, default: %{})
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:name, :type, :params])
    |> Utils.string_key_to_atom_map(:params)
    |> validate_required([:name, :type, :params])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      connection = Repo.Connection.create!(command)

      type = String.to_atom(command.type)

      if type == Utils.postgresql_type() do
        {:ok, command} =
          Service.PostgresqlConnection.Create.new(
            Map.merge(command.params, %{
              connection_id: connection.id
            })
          )

        {:ok, postgresql_connection} = Service.PostgresqlConnection.Create.handle(command)
        {connection, postgresql_connection}
      else
        raise "Invalid connection type #{connection.type}"
      end
    end)
  end
end
