defmodule Aicacia.DataVault.Service.Connection.Repo.Connection do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.Connection
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  def get(id), do: Repo.get(Connection, id)
  def get!(id), do: Repo.get!(Connection, id)
  def get_by(search), do: Repo.get_by(Connection, search)
  def get_by!(search), do: Repo.get_by!(Connection, search)

  def all(),
    do:
      Repo.all(Connection)
      |> Enum.map(fn connection ->
        {connection, get_type!(connection)}
      end)

  def get_with_type!(id) do
    connection = get!(id)
    {connection, get_type!(connection)}
  end

  def get_type!(%Connection{id: id, type: type}) do
    if type == to_string(Utils.postgresql_type()) do
      Service.PostgresqlConnection.Repo.PostgresqlConnection.get_by!(connection_id: id)
    else
      raise "Invalid connection type #{type}"
    end
  end

  def create!(%{} = attrs) do
    %Connection{}
    |> cast(attrs, [:name, :type])
    |> validate_required([:name, :type])
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%Connection{} = connection, attrs \\ %{}) do
    connection
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> Repo.update!()
  end

  def delete!(%Connection{} = connection) do
    Repo.delete!(connection)
  end
end
