defmodule Aicacia.DataVault.Service.MapperField.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.MapperField.Delete
  alias Aicacia.DataVault.Service.MapperField.Repo

  schema "" do
    field(:mapper_field_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:mapper_field_id])
    |> validate_required([:mapper_field_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      mapper_field = Repo.MapperField.get!(command.mapper_field_id)

      Repo.MapperField.delete!(mapper_field)

      mapper_field
    end)
  end
end
