defmodule Aicacia.DataVault.Service.MapperField.Repo.MapperField do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.MapperField

  def get(id), do: Repo.get(MapperField, id)
  def get!(id), do: Repo.get!(MapperField, id)
  def get_by(search), do: Repo.get_by(MapperField, search)
  def get_by!(search), do: Repo.get_by!(MapperField, search)

  def get_query(id) do
    from(mf in MapperField,
      where: mf.id == ^id,
      preload: [:to],
      select: mf
    )
  end

  def get_all_subquery() do
    from(mf in MapperField,
      preload: [:to],
      select: mf
    )
  end

  def create!(%{} = attrs) do
    %MapperField{}
    |> cast(attrs, [:from, :to_id, :mapper_id])
    |> validate_required([:from, :to_id, :mapper_id])
    |> foreign_key_constraint(:mapper_id)
    |> foreign_key_constraint(:to_id)
    |> unique_constraint(:name, name: :mapper_fields_mapper_id_from_to_id)
    |> Repo.insert!()
  end

  def update!(%MapperField{} = mapper_field, attrs \\ %{}) do
    mapper_field
    |> cast(attrs, [:from, :to_id])
    |> validate_required([:from, :to_id])
    |> foreign_key_constraint(:to_id)
    |> unique_constraint(:name, name: :mapper_fields_mapper_id_from_to_id)
    |> Repo.update!()
  end

  def delete!(%MapperField{} = mapper_field) do
    Repo.delete!(mapper_field)
  end
end
