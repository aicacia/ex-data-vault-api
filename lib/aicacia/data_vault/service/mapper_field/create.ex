defmodule Aicacia.DataVault.Service.MapperField.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.MapperField.Create
  alias Aicacia.DataVault.Service.MapperField.Repo

  schema "" do
    field(:from, :string)
    field(:to_id, :id)
    field(:mapper_id, :id)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:from, :to_id, :mapper_id])
    |> validate_required([:from, :to_id, :mapper_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      Repo.MapperField.create!(command)
    end)
  end
end
