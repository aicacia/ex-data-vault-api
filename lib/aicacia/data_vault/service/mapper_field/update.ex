defmodule Aicacia.DataVault.Service.MapperField.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.MapperField.Update
  alias Aicacia.DataVault.Service.MapperField.Repo

  schema "" do
    field(:mapper_field_id, :id)
    field(:from, :string)
    field(:to_id, :id)
    field(:mapper_id, :id)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:mapper_field_id, :from, :to_id, :mapper_id])
    |> validate_required([:mapper_field_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      mapper_field = Repo.MapperField.get!(command.mapper_field_id)

      Repo.MapperField.update!(
        mapper_field,
        command
      )
    end)
  end
end
