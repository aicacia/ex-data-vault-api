defmodule Aicacia.DataVault.Service.Mapper.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Mapper.Create
  alias Aicacia.DataVault.Service.Mapper.Repo
  alias Aicacia.DataVault.Service

  schema "" do
    field(:extractor_id, :id)
    field(:table_id, :id)
    field(:name, :string)
    field(:fields, {:array, :map}, default: [])
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:extractor_id, :table_id, :name, :fields])
    |> validate_required([:extractor_id, :table_id, :name, :fields])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      mapper = Repo.Mapper.create!(command)
      table = Service.Table.Repo.Table.get_full!(mapper.table_id)

      Enum.each(command.fields, fn field_params ->
        {:ok, field_command} =
          Service.MapperField.Create.new(Map.merge(field_params, %{mapper_id: mapper.id}))

        if Enum.any?(table.fields, fn table_field -> table_field.id == field_command.to_id end) do
          {:ok, field} = Service.MapperField.Create.handle(field_command)
          field
        else
          raise "field to_id must be in the mappers table"
        end
      end)

      Repo.Mapper.get_full!(mapper.id)
    end)
  end
end
