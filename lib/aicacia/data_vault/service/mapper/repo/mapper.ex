defmodule Aicacia.DataVault.Service.Mapper.Repo.Mapper do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.Mapper
  alias Aicacia.DataVault.Service

  def get(id), do: Repo.get(Mapper, id)
  def get!(id), do: Repo.get!(Mapper, id)
  def get_by(search), do: Repo.get_by(Mapper, search)
  def get_by!(search), do: Repo.get_by!(Mapper, search)

  def all_query(extractor_id) do
    fields_query = Service.MapperField.Repo.MapperField.get_all_subquery()

    from(u in Mapper,
      where: u.extractor_id == ^extractor_id,
      preload: [fields: ^fields_query],
      select: u
    )
  end

  def all(extractor_id),
    do:
      all_query(extractor_id)
      |> Repo.all()

  def get_full!(id), do: get_by_id_query(id) |> Repo.one!()

  def get_by_id_query(id) do
    fields_query = Service.MapperField.Repo.MapperField.get_all_subquery()

    from(u in Mapper,
      where: u.id == ^id,
      preload: [fields: ^fields_query],
      select: u
    )
  end

  def create!(%{} = attrs) do
    %Mapper{}
    |> cast(attrs, [:extractor_id, :table_id, :name])
    |> validate_required([:extractor_id, :table_id, :name])
    |> foreign_key_constraint(:extractor_id)
    |> foreign_key_constraint(:table_id)
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%Mapper{} = mapper, attrs \\ %{}) do
    mapper
    |> cast(attrs, [:extractor_id, :name])
    |> validate_required([:extractor_id, :name])
    |> foreign_key_constraint(:extractor_id)
    |> unique_constraint(:name)
    |> Repo.update!()
  end

  def delete!(%Mapper{} = mapper) do
    Repo.delete!(mapper)
  end
end
