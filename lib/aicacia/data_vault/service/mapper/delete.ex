defmodule Aicacia.DataVault.Service.Mapper.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Mapper.Delete
  alias Aicacia.DataVault.Service.Mapper.Repo

  schema "" do
    field(:mapper_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:mapper_id])
    |> validate_required([:mapper_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      mapper = Repo.Mapper.get!(command.mapper_id)

      Repo.Mapper.delete!(mapper)

      mapper
    end)
  end
end
