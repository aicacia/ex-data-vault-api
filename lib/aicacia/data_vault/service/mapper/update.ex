defmodule Aicacia.DataVault.Service.Mapper.Update do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Mapper.Update
  alias Aicacia.DataVault.Service.Mapper.Repo

  schema "" do
    field(:extractor_id, :id)
    field(:mapper_id, :id)
    field(:name, :string)
  end

  def changeset(%{} = params) do
    %Update{}
    |> cast(params, [:extractor_id, :mapper_id, :name])
    |> validate_required([:extractor_id, :mapper_id, :name])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      mapper = Repo.Mapper.get!(command.mapper_id)

      Repo.Mapper.update!(
        mapper,
        command
      )
    end)
  end
end
