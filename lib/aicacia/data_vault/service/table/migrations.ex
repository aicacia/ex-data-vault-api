defmodule Aicacia.DataVault.Service.Table.Migrations do
  use Ecto.Schema
  use Ecto.Migration

  alias Aicacia.DataVault.Model.{Table, TableField}

  def create_table!(%Table{name: table_name, fields: fields}) do
    create table(table_name, primary_key: false) do
      Enum.each(fields, fn field ->
        add(String.to_atom(field.name), String.to_atom(field.type), null: !field.primary_key)
      end)

      timestamps(type: :utc_datetime, updated_at: false)
    end

    primary_key =
      fields
      |> Enum.find(fn field -> field.primary_key end)

    create(index(table_name, [primary_key.name]))

    fields
    |> Enum.filter(fn field -> field.reference_id != nil end)
    |> Enum.each(fn field ->
      create(index(table_name, [field.name]))
    end)

    tracked_fields =
      fields
      |> Enum.filter(fn field -> field.tracked end)
      |> Enum.map(fn field -> String.to_atom(field.name) end)

    create(unique_index(table_name, tracked_fields, name: get_tracked_index_name(table_name)))
  end

  def add_table_column!(%Table{name: table_name, fields: fields}, %TableField{
        name: field_name,
        type: field_type,
        tracked: tracked
      }) do
    alter table(table_name) do
      add(String.to_atom(field_name), String.to_atom(field_type), null: true)
    end

    if tracked do
      tracked_fields =
        fields
        |> Enum.filter(fn field -> !field.tracked end)
        |> Enum.map(fn field -> String.to_atom(field.name) end)

      drop(unique_index(table_name, [], name: :tracked_index))

      create(unique_index(table_name, tracked_fields, name: get_tracked_index_name(table_name)))
    end
  end

  def delete_table!(%Table{name: table_name}) do
    drop(table(table_name))
    drop(unique_index(table_name, [], name: get_tracked_index_name(table_name)))
  end

  def delete_table_column!(%Table{name: table_name}, %TableField{name: field_name}) do
    alter table(table_name) do
      remove(String.to_atom(field_name))
    end
  end

  def get_tracked_index_name(table_name) do
    String.to_atom("#{table_name}_tracked_index")
  end
end
