defmodule Aicacia.DataVault.Service.Table.Create do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Table.Create
  alias Aicacia.DataVault.Service.Table.Repo
  alias Aicacia.DataVault.Service

  schema "" do
    field(:name, :string)
    field(:fields, {:array, :map}, default: [])
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:name, :fields])
    |> validate_required([:name, :fields])
    |> validate_tracked()
  end

  defp validate_tracked(%Ecto.Changeset{} = changeset) do
    fields = get_field(changeset, :fields)

    if Enum.any?(fields, fn field -> Map.get(field, :tracked) end) do
      changeset
    else
      changeset
      |> add_error(:fields, "fields do not contain any tracked fields", validation: :fields)
    end
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      table = Repo.Table.create!(command)

      Enum.map(command.fields, fn field_params ->
        {:ok, field_command} =
          Service.TableField.Create.new(Map.merge(field_params, %{table_id: table.id}))

        {:ok, field} = Service.TableField.Create.handle(field_command)
        field
      end)

      table = Repo.Table.get_full!(table.id)

      Service.Table.Migrations.create_table!(table)

      table
    end)
  end
end
