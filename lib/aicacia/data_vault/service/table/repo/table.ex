defmodule Aicacia.DataVault.Service.Table.Repo.Table do
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Aicacia.DataVault.Repo
  alias Aicacia.DataVault.Model.Table
  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  def get(id), do: Repo.get(Table, id)
  def get!(id), do: Repo.get!(Table, id)
  def get_by(search), do: Repo.get_by(Table, search)
  def get_by!(search), do: Repo.get_by!(Table, search)

  def all(),
    do:
      from(t in Table,
        preload: [:fields],
        select: t
      )
      |> Repo.all()

  def get_full!(id), do: get_by_id_query(id) |> Repo.one!()

  def get_by_id_query(id) do
    fields_query = Service.TableField.Repo.TableField.get_all_query()

    from(u in Table,
      where: u.id == ^id,
      preload: [fields: ^fields_query],
      select: u
    )
  end

  def create!(%{} = attrs) do
    %Table{}
    |> cast(attrs, [:name])
    |> Utils.validate_valid_db_name(:name)
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%Table{} = table, attrs \\ %{}) do
    table
    |> cast(attrs, [:name])
    |> Utils.validate_valid_db_name(:name)
    |> unique_constraint(:name)
    |> Repo.update!()
  end

  def delete!(%Table{} = table) do
    Repo.delete!(table)
  end
end
