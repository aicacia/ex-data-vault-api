defmodule Aicacia.DataVault.Service.Table.Delete do
  use Aicacia.Handler

  alias Aicacia.DataVault.Service.Table.Delete
  alias Aicacia.DataVault.Service.Table.Repo
  alias Aicacia.DataVault.Service

  schema "" do
    field(:table_id, :id)
  end

  def changeset(%{} = params) do
    %Delete{}
    |> cast(params, [:table_id])
    |> validate_required([:table_id])
  end

  def handle(%{} = command) do
    Aicacia.DataVault.Repo.transaction(fn ->
      table = Repo.Table.get!(command.table_id)

      Service.Table.Migrations.delete_table!(table)
      Repo.Table.delete!(table)

      table
    end)
  end
end
