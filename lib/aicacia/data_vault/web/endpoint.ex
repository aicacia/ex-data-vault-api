defmodule Aicacia.DataVault.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :aicacia_data_vault

  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  plug CORSPlug

  plug Aicacia.DataVault.Web.Router
end
