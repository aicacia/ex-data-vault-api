defmodule Aicacia.DataVault.Web.Router do
  use Aicacia.DataVault.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Aicacia.DataVault.Web.Controller do
    pipe_through :api

    resources "/connections", Connection, only: [:index, :show, :create, :update, :delete] do
      resources "/extractors", Extractor, only: [:index, :show, :create, :update, :delete] do
        resources "/mappers", Mapper, only: [:index, :show, :create, :update, :delete]
      end
    end

    resources "/tables", Table, only: [:index, :show, :create, :delete]
    resources "/cron-jobs", CronJob, only: [:index, :show, :create, :update, :delete]
  end
end
