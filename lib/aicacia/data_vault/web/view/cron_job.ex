defmodule Aicacia.DataVault.Web.View.CronJob do
  use Aicacia.DataVault.Web, :view
  alias Aicacia.DataVault.Web.View.CronJob

  def render("index.json", %{cron_jobs: cron_jobs}) do
    render_many(cron_jobs, CronJob, "cron_job.json")
  end

  def render("show.json", %{cron_job: cron_job}) do
    render_one(cron_job, CronJob, "cron_job.json")
  end

  def render("cron_job.json", %{cron_job: cron_job}) do
    %{
      id: cron_job.id,
      name: cron_job.name,
      schedule: cron_job.schedule,
      inserted_at: cron_job.inserted_at,
      updated_at: cron_job.updated_at
    }
  end
end
