defmodule Aicacia.DataVault.Web.View.ErrorHelpers do
  def translate_error({msg, opts}) do
    if count = opts[:count] do
      Gettext.dngettext(Aicacia.DataVault.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(Aicacia.DataVault.Gettext, "errors", msg, opts)
    end
  end
end
