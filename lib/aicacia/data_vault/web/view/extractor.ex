defmodule Aicacia.DataVault.Web.View.Extractor do
  use Aicacia.DataVault.Web, :view
  alias Aicacia.DataVault.Web.View.Extractor

  alias Aicacia.DataVault.Service.Utils

  def render("index.json", %{extractors: extractors}) do
    render_many(extractors, Extractor, "extractor.json")
  end

  def render("show.json", %{extractor: extractor, extractor_type: extractor_type}) do
    render_one(extractor, Extractor, "extractor.json", %{extractor_type: extractor_type})
  end

  def render("extractor.json", %{extractor: {extractor, extractor_type}}) do
    render("extractor.json", %{extractor: extractor, extractor_type: extractor_type})
  end

  def render("extractor.json", %{extractor: extractor, extractor_type: extractor_type}) do
    Map.merge(
      render("extractor.json", %{extractor: extractor}),
      render_extractor_type(String.to_atom(extractor.type), extractor_type)
    )
  end

  def render("extractor.json", %{extractor: extractor}) do
    %{
      id: extractor.id,
      connection_id: extractor.connection_id,
      name: extractor.name,
      type: extractor.type,
      inserted_at: extractor.inserted_at,
      updated_at: extractor.updated_at
    }
  end

  def render_extractor_type(type, extractor_type) do
    if type == Utils.postgresql_type() do
      %{
        query: extractor_type.query,
        inserted_at: extractor_type.inserted_at,
        updated_at: extractor_type.updated_at
      }
    else
      %{}
    end
  end
end
