defmodule Aicacia.DataVault.Web.View.Mapper do
  use Aicacia.DataVault.Web, :view
  alias Aicacia.DataVault.Web.View.Mapper

  def render("index.json", %{mappers: mappers}) do
    render_many(mappers, Mapper, "mapper.json")
  end

  def render("show.json", %{mapper: mapper}) do
    render_one(mapper, Mapper, "mapper.json")
  end

  def render("mapper.json", %{mapper: mapper}) do
    %{
      id: mapper.id,
      table_id: mapper.table_id,
      extractor_id: mapper.extractor_id,
      name: mapper.name,
      fields:
        Enum.map(mapper.fields, fn mapper_field ->
          render("mapper_field.json", %{mapper_field: mapper_field})
        end),
      inserted_at: mapper.inserted_at,
      updated_at: mapper.updated_at
    }
  end

  def render("mapper_field.json", %{mapper_field: mapper_field}) do
    %{
      id: mapper_field.id,
      from: mapper_field.from,
      to_id: mapper_field.to_id,
      inserted_at: mapper_field.inserted_at,
      updated_at: mapper_field.updated_at
    }
  end
end
