defmodule Aicacia.DataVault.Web.View.Connection do
  use Aicacia.DataVault.Web, :view
  alias Aicacia.DataVault.Web.View.Connection

  alias Aicacia.DataVault.Service.Utils

  def render("index.json", %{connections: connections}) do
    render_many(connections, Connection, "connection.json")
  end

  def render("show.json", %{connection: connection, connection_type: connection_type}) do
    render_one(connection, Connection, "connection.json", %{connection_type: connection_type})
  end

  def render("connection.json", %{connection: {connection, connection_type}}) do
    render("connection.json", %{connection: connection, connection_type: connection_type})
  end

  def render("connection.json", %{connection: connection, connection_type: connection_type}) do
    Map.merge(
      render("connection.json", %{connection: connection}),
      render_connection_type(String.to_atom(connection.type), connection_type)
    )
  end

  def render("connection.json", %{connection: connection}) do
    %{
      id: connection.id,
      name: connection.name,
      type: connection.type,
      inserted_at: connection.inserted_at,
      updated_at: connection.updated_at
    }
  end

  def render_connection_type(type, connection_type) do
    if type == Utils.postgresql_type() do
      %{
        username: connection_type.username,
        password: connection_type.password,
        uri: connection_type.uri,
        inserted_at: connection_type.inserted_at,
        updated_at: connection_type.updated_at
      }
    else
      %{}
    end
  end
end
