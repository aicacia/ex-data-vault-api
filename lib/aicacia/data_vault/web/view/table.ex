defmodule Aicacia.DataVault.Web.View.Table do
  use Aicacia.DataVault.Web, :view
  alias Aicacia.DataVault.Web.View.Table

  def render("index.json", %{tables: tables}) do
    render_many(tables, Table, "table.json")
  end

  def render("show.json", %{table: table}) do
    render_one(table, Table, "table.json")
  end

  def render("table.json", %{table: table}) do
    %{
      id: table.id,
      name: table.name,
      fields:
        Enum.map(table.fields, fn table_field ->
          render("table_field.json", %{table_field: table_field})
        end),
      inserted_at: table.inserted_at,
      updated_at: table.updated_at
    }
  end

  def render("table_field.json", %{table_field: table_field}) do
    %{
      id: table_field.id,
      name: table_field.name,
      type: table_field.type,
      tracked: table_field.tracked,
      primary_key: table_field.primary_key,
      inserted_at: table_field.inserted_at,
      updated_at: table_field.updated_at
    }
    |> Map.merge(render_reference(table_field))
  end

  def render_reference(%{reference: %Ecto.Association.NotLoaded{}}), do: %{}

  def render_reference(%{reference: reference}),
    do: render("table_field.json", %{table_field: reference})
end
