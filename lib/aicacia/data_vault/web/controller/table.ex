defmodule Aicacia.DataVault.Web.Controller.Table do
  use Aicacia.DataVault.Web, :controller

  alias Aicacia.DataVault.Service.Table.{Create, Delete}

  action_fallback Aicacia.DataVault.Web.Controller.Fallback

  def index(conn, _params) do
    tables = Aicacia.DataVault.Service.Table.Repo.Table.all()

    conn
    |> put_view(Aicacia.DataVault.Web.View.Table)
    |> render("index.json", tables: tables)
  end

  def show(conn, %{"id" => table_id}) do
    table = Aicacia.DataVault.Service.Table.Repo.Table.get_full!(table_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Table)
    |> render("show.json", table: table)
  end

  def create(conn, params) do
    with {:ok, command} <- Create.new(params),
         {:ok, table} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Table)
      |> render("show.json", table: table)
    end
  end

  def delete(conn, %{"id" => table_id} = params) do
    with {:ok, command} <-
           Delete.new(Map.merge(params, %{"table_id" => table_id})),
         {:ok, table} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Table)
      |> render("show.json", table: table)
    end
  end
end
