defmodule Aicacia.DataVault.Web.Controller.Extractor do
  use Aicacia.DataVault.Web, :controller

  alias Aicacia.DataVault.Service.Extractor.{Create, Update, Delete}

  action_fallback Aicacia.DataVault.Web.Controller.Fallback

  def index(conn, %{"connection_id" => connection_id}) do
    extractors = Aicacia.DataVault.Service.Extractor.Repo.Extractor.all(connection_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Extractor)
    |> render("index.json", extractors: extractors)
  end

  def show(conn, %{"id" => extractor_id}) do
    {extractor, extractor_type} =
      Aicacia.DataVault.Service.Extractor.Repo.Extractor.get_with_type!(extractor_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Extractor)
    |> render("show.json", extractor: extractor, extractor_type: extractor_type)
  end

  def create(conn, params) do
    with {:ok, command} <- Create.new(params),
         {:ok, extractor} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Extractor)
      |> render("show.json", extractor: extractor)
    end
  end

  def update(conn, %{"id" => extractor_id} = params) do
    with {:ok, command} <-
           Update.new(Map.merge(params, %{"extractor_id" => extractor_id})),
         {:ok, extractor} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Extractor)
      |> render("show.json", extractor: extractor)
    end
  end

  def delete(conn, %{"id" => extractor_id} = params) do
    with {:ok, command} <-
           Delete.new(Map.merge(params, %{"extractor_id" => extractor_id})),
         {:ok, extractor} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Extractor)
      |> render("show.json", extractor: extractor)
    end
  end
end
