defmodule Aicacia.DataVault.Web.Controller.Mapper do
  use Aicacia.DataVault.Web, :controller

  alias Aicacia.DataVault.Service.Mapper.{Create, Update, Delete}

  action_fallback Aicacia.DataVault.Web.Controller.Fallback

  def index(conn, %{"extractor_id" => extractor_id}) do
    mappers = Aicacia.DataVault.Service.Mapper.Repo.Mapper.all(extractor_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Mapper)
    |> render("index.json", mappers: mappers)
  end

  def show(conn, %{"id" => mapper_id}) do
    mapper = Aicacia.DataVault.Service.Mapper.Repo.Mapper.get_full!(mapper_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Mapper)
    |> render("show.json", mapper: mapper)
  end

  def create(conn, params) do
    with {:ok, command} <- Create.new(params),
         {:ok, mapper} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Mapper)
      |> render("show.json", mapper: mapper)
    end
  end

  def update(conn, %{"id" => mapper_id} = params) do
    with {:ok, command} <-
           Update.new(Map.merge(params, %{"mapper_id" => mapper_id})),
         {:ok, mapper} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Mapper)
      |> render("show.json", mapper: mapper)
    end
  end

  def delete(conn, %{"id" => mapper_id} = params) do
    with {:ok, command} <-
           Delete.new(Map.merge(params, %{"mapper_id" => mapper_id})),
         {:ok, mapper} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Mapper)
      |> render("show.json", mapper: mapper)
    end
  end
end
