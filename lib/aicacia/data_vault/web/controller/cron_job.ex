defmodule Aicacia.DataVault.Web.Controller.CronJob do
  use Aicacia.DataVault.Web, :controller

  alias Aicacia.DataVault.Service.CronJob.{Create, Update, Delete}

  action_fallback Aicacia.DataVault.Web.Controller.Fallback

  def index(conn, _params) do
    cron_jobs = Aicacia.DataVault.Service.CronJob.Repo.CronJob.all()

    conn
    |> put_view(Aicacia.DataVault.Web.View.CronJob)
    |> render("index.json", cron_jobs: cron_jobs)
  end

  def show(conn, %{"id" => cron_job_id}) do
    cron_job = Aicacia.DataVault.Service.CronJob.Repo.CronJob.get!(cron_job_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.CronJob)
    |> render("show.json", cron_job: cron_job)
  end

  def create(conn, params) do
    with {:ok, command} <- Create.new(params),
         {:ok, cron_job} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.CronJob)
      |> render("show.json", cron_job: cron_job)
    end
  end

  def update(conn, %{"id" => cron_job_id} = params) do
    with {:ok, command} <- Update.new(params |> Map.merge(%{"cron_job_id" => cron_job_id})),
         {:ok, cron_job} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.CronJob)
      |> render("show.json", cron_job: cron_job)
    end
  end

  def delete(conn, %{"id" => cron_job_id} = params) do
    with {:ok, command} <-
           Delete.new(Map.merge(params, %{"cron_job_id" => cron_job_id})),
         {:ok, cron_job} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.CronJob)
      |> render("show.json", cron_job: cron_job)
    end
  end
end
