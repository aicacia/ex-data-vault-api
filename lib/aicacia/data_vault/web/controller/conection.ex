defmodule Aicacia.DataVault.Web.Controller.Connection do
  use Aicacia.DataVault.Web, :controller

  alias Aicacia.DataVault.Service.Connection.{Create, Update, Delete}

  action_fallback Aicacia.DataVault.Web.Controller.Fallback

  def index(conn, _params) do
    connections = Aicacia.DataVault.Service.Connection.Repo.Connection.all()

    conn
    |> put_view(Aicacia.DataVault.Web.View.Connection)
    |> render("index.json", connections: connections)
  end

  def show(conn, %{"id" => connection_id}) do
    {connection, connection_type} =
      Aicacia.DataVault.Service.Connection.Repo.Connection.get_with_type!(connection_id)

    conn
    |> put_view(Aicacia.DataVault.Web.View.Connection)
    |> render("show.json", connection: connection, connection_type: connection_type)
  end

  def create(conn, params) do
    with {:ok, command} <- Create.new(params),
         {:ok, {connection, connection_type}} <- Create.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Connection)
      |> put_status(201)
      |> render("show.json", connection: connection, connection_type: connection_type)
    end
  end

  def update(conn, %{"id" => connection_id} = params) do
    with {:ok, command} <-
           Update.new(Map.merge(params, %{"connection_id" => connection_id})),
         {:ok, {connection, connection_type}} <- Update.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Connection)
      |> put_status(200)
      |> render("show.json", connection: connection, connection_type: connection_type)
    end
  end

  def delete(conn, %{"id" => connection_id} = params) do
    with {:ok, command} <-
           Delete.new(Map.merge(params, %{"connection_id" => connection_id})),
         {:ok, connection} <- Delete.handle(command) do
      conn
      |> put_view(Aicacia.DataVault.Web.View.Connection)
      |> put_status(200)
      |> render("show.json", connection: connection)
    end
  end
end
