defmodule Aicacia.DataVault.Web do
  def controller do
    quote do
      use Phoenix.Controller, namespace: Aicacia.DataVault.Web

      import Plug.Conn
      import Aicacia.DataVault.Gettext
      alias Aicacia.DataVault.Web.Router.Helpers, as: Routes
    end
  end

  def plugs do
    quote do
      use Phoenix.Controller, namespace: Aicacia.DataVault.Web
      import Plug.Conn
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/auth_web/templates",
        namespace: Aicacia.DataVault.Web

      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      import Aicacia.DataVault.Web.View.ErrorHelpers
      import Aicacia.DataVault.Gettext
      alias Aicacia.DataVault.Web.Router.Helpers, as: Routes
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import Aicacia.DataVault.Gettext
    end
  end

  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
