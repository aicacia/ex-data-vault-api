defmodule Aicacia.DataVault.Scheduler do
  use Quantum.Scheduler,
    otp_app: :aicacia_data_vault
end
