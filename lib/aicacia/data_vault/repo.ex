defmodule Aicacia.DataVault.Repo do
  use Ecto.Repo,
    otp_app: :aicacia_data_vault,
    adapter: Ecto.Adapters.Postgres
end
