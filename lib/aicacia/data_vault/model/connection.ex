defmodule Aicacia.DataVault.Model.Connection do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.Extractor
  alias Aicacia.DataVault.Service.Utils

  schema "connections" do
    has_many(:extractors, Extractor)

    field(:name, :string)
    field(:type, :string, default: Utils.postgresql_type())

    timestamps(type: :utc_datetime)
  end
end
