defmodule Aicacia.DataVault.Model.TableField do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.{Table, TableField}

  schema "table_fields" do
    belongs_to(:table, Table)
    belongs_to(:reference, TableField)

    field(:name, :string)
    field(:type, :string, default: "string")
    field(:tracked, :boolean, default: true)
    field(:primary_key, :boolean, default: false)

    timestamps(type: :utc_datetime)
  end
end
