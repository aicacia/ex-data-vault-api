defmodule Aicacia.DataVault.Model.Extractor do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.{CronJob, Mapper, Connection}
  alias Aicacia.DataVault.Service.Utils

  schema "extractors" do
    belongs_to(:connection, Connection)
    belongs_to(:cron_job, CronJob)
    has_many(:mappers, Mapper)

    field(:name, :string)
    field(:type, :string, default: Utils.postgresql_type())
    field(:locked, :boolean, default: false)

    timestamps(type: :utc_datetime)
  end
end
