defmodule Aicacia.DataVault.Model.PostgresqlExtractor do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.Extractor

  schema "postgresql_extractors" do
    belongs_to(:extractor, Extractor)

    field(:query, :string)

    timestamps(type: :utc_datetime)
  end
end
