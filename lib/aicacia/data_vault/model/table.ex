defmodule Aicacia.DataVault.Model.Table do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.TableField

  schema "tables" do
    has_many(:fields, TableField)

    field(:name, :string)

    timestamps(type: :utc_datetime)
  end
end
