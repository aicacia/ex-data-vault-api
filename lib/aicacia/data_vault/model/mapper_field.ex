defmodule Aicacia.DataVault.Model.MapperField do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.{Mapper, TableField}

  schema "mapper_fields" do
    belongs_to(:mapper, Mapper)
    belongs_to(:to, TableField)

    field(:from, :string)

    timestamps(type: :utc_datetime)
  end
end
