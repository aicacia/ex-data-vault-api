defmodule Aicacia.DataVault.Model.CronJob do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.Extractor

  schema "cron_jobs" do
    has_many(:extractors, Extractor)

    field(:name, :string)
    field(:schedule, :string, default: "0 0 * * *")

    timestamps(type: :utc_datetime)
  end
end
