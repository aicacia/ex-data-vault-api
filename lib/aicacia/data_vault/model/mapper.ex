defmodule Aicacia.DataVault.Model.Mapper do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.{MapperField, Extractor, Table}

  schema "mappers" do
    belongs_to(:extractor, Extractor)
    belongs_to(:table, Table)
    has_many(:fields, MapperField)

    field(:name, :string)

    timestamps(type: :utc_datetime)
  end
end
