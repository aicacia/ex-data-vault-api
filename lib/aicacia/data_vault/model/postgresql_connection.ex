defmodule Aicacia.DataVault.Model.PostgresqlConnection do
  use Ecto.Schema

  alias Aicacia.DataVault.Model.Connection

  schema "postgresql_connections" do
    belongs_to(:connection, Connection)

    field(:username, :string)
    field(:password, :string)
    field(:uri, :string)

    timestamps(type: :utc_datetime)
  end
end
