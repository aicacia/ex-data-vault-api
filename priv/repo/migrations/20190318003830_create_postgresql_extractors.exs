defmodule Aicacia.DataVault.Repo.Migrations.CreatePostgresqlExtractors do
  use Ecto.Migration

  def change do
    create table(:postgresql_extractors) do
      add(
        :extractor_id,
        references(:extractors, on_delete: :delete_all),
        null: false
      )

      add(:query, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:postgresql_extractors, [:extractor_id]))
  end
end
