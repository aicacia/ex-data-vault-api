defmodule Aicacia.DataVault.Repo.Migrations.CreateTableFields do
  use Ecto.Migration

  def change do
    create table(:table_fields) do
      add(
        :table_id,
        references(:tables, on_delete: :delete_all),
        null: false
      )

      add(
        :reference_id,
        references(:table_fields, on_delete: :delete_all),
        null: true
      )

      add(:name, :string, null: false)
      add(:type, :string, null: false, default: "string")
      add(:tracked, :boolean, null: false, default: true)
      add(:primary_key, :boolean, null: false, default: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:table_fields, [:table_id]))
    create(index(:table_fields, [:reference_id]))
    create(unique_index(:table_fields, [:table_id, :name]))
  end
end
