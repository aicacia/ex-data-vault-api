defmodule Aicacia.DataVault.Repo.Migrations.CreateExtractors do
  use Ecto.Migration

  def change do
    create table(:extractors) do
      add(
        :connection_id,
        references(:connections, on_delete: :nothing),
        null: false
      )

      add(
        :cron_job_id,
        references(:cron_jobs, on_delete: :nothing),
        null: false
      )

      add(:name, :string, null: false)
      add(:type, :string, null: false)
      add(:locked, :boolean, null: false, default: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:extractors, [:connection_id]))
    create(unique_index(:extractors, [:name]))
  end
end
