defmodule Aicacia.DataVault.Repo.Migrations.CreateMappers do
  use Ecto.Migration

  def change do
    create table(:mappers) do
      add(
        :extractor_id,
        references(:extractors, on_delete: :delete_all),
        null: false
      )

      add(
        :table_id,
        references(:tables, on_delete: :delete_all),
        null: false
      )

      add(:name, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:mappers, [:extractor_id]))
    create(index(:mappers, [:table_id]))
    create(unique_index(:mappers, [:name]))
  end
end
