defmodule Aicacia.DataVault.Repo.Migrations.CreatePostgresqlConnections do
  use Ecto.Migration

  def change do
    create table(:postgresql_connections) do
      add(
        :connection_id,
        references(:connections, on_delete: :delete_all),
        null: false
      )

      add(:username, :string, null: false)
      add(:password, :string, null: false)
      add(:uri, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:postgresql_connections, [:connection_id]))
  end
end
