defmodule Aicacia.DataVault.Repo.Migrations.InitialSeeds do
  use Ecto.Migration

  alias Aicacia.DataVault.Service
  alias Aicacia.DataVault.Service.Utils

  def up do
    Aicacia.DataVault.Repo.transaction(fn ->
      {connection, _connection_type} =
        Service.Connection.Create.handle!(
          Service.Connection.Create.new!(%{
            name: "DataVault Connection",
            type: to_string(Utils.postgresql_type()),
            params: %{
              username:
                Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:username],
              password:
                Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:password],
              uri:
                "postgresql://#{
                  Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:hostname]
                }:5432/#{
                  Application.get_env(:aicacia_data_vault, Aicacia.DataVault.Repo)[:database]
                }"
            }
          })
        )

      cron_job =
        Service.CronJob.Create.handle!(
          Service.CronJob.Create.new!(%{
            name: "Every day at midnight",
            schedule: "0 0 * * *"
          })
        )

      connections_table =
        Service.Table.Create.handle!(
          Service.Table.Create.new!(%{
            name: "tracked_connections",
            fields: [
              %{
                name: "id",
                type: "id",
                primary_key: true,
                tracked: false
              },
              %{
                name: "name",
                type: "string",
                tracked: true
              },
              %{
                name: "type",
                type: "string",
                tracked: true
              }
            ]
          })
        )

      connections_id_field =
        connections_table.fields
        |> Enum.find(fn field ->
          field.name == "id"
        end)

      postgresql_connections_table =
        Service.Table.Create.handle!(
          Service.Table.Create.new!(%{
            name: "tracked_postgresql_connections",
            fields: [
              %{
                name: "id",
                type: "id",
                primary_key: true,
                tracked: false
              },
              %{
                name: "connection_id",
                type: "id",
                reference_id: connections_id_field.id,
                tracked: true
              },
              %{
                name: "uri",
                type: "string",
                tracked: true
              },
              %{
                name: "username",
                type: "string",
                tracked: true
              },
              %{
                name: "password",
                type: "string",
                tracked: true
              }
            ]
          })
        )

      postgresql_connections_extractor =
        Service.Extractor.Create.handle!(
          Service.Extractor.Create.new!(%{
            connection_id: connection.id,
            cron_job_id: cron_job.id,
            name: "Postgresql Connections Extractor",
            type: to_string(Utils.postgresql_type()),
            params: %{
              query:
                "SELECT id, connection_id, uri, username, password FROM postgresql_connections"
            }
          })
        )

      _postgresql_connections_mapper =
        Service.Mapper.Create.handle!(
          Service.Mapper.Create.new!(%{
            name: "Postgresql Connections Mapper",
            table_id: postgresql_connections_table.id,
            extractor_id: postgresql_connections_extractor.id,
            fields:
              Enum.map(postgresql_connections_table.fields, fn table_field ->
                %{
                  name: table_field.name,
                  from: table_field.name,
                  to_id: table_field.id
                }
              end)
          })
        )

      connections_extractor =
        Service.Extractor.Create.handle!(
          Service.Extractor.Create.new!(%{
            connection_id: connection.id,
            cron_job_id: cron_job.id,
            name: "Connections Extractor",
            type: to_string(Utils.postgresql_type()),
            params: %{
              query: "SELECT id, name, type FROM connections"
            }
          })
        )

      _connections_mapper =
        Service.Mapper.Create.handle!(
          Service.Mapper.Create.new!(%{
            name: "Connections Mapper",
            table_id: connections_table.id,
            extractor_id: connections_extractor.id,
            fields:
              Enum.map(connections_table.fields, fn table_field ->
                %{
                  name: table_field.name,
                  from: table_field.name,
                  to_id: table_field.id
                }
              end)
          })
        )
    end)
  end
end
