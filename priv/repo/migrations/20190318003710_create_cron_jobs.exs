defmodule Aicacia.DataVault.Repo.Migrations.CreateCronJobs do
  use Ecto.Migration

  def change do
    create table(:cron_jobs) do
      add(:name, :string, null: false)
      add(:schedule, :string, default: "0 0 * * *")

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:cron_jobs, [:name]))
  end
end
