defmodule Aicacia.DataVault.Repo.Migrations.CreateTablesAndTableFields do
  use Ecto.Migration

  def change do
    create table(:tables) do
      add(:name, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:tables, [:name]))
  end
end
