defmodule Aicacia.DataVault.Repo.Migrations.CreateMapperFields do
  use Ecto.Migration

  def change do
    create table(:mapper_fields) do
      add(
        :mapper_id,
        references(:mappers, on_delete: :delete_all),
        null: false
      )

      add(
        :to_id,
        references(:table_fields, on_delete: :delete_all),
        null: false
      )

      add(:from, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(index(:mapper_fields, [:mapper_id]))
    create(index(:mapper_fields, [:to_id]))
    create(unique_index(:mapper_fields, [:mapper_id, :from, :to_id]))
  end
end
